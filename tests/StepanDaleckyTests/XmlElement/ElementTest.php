<?php
declare(strict_types=1);

namespace StepanDaleckyTests\XmlElement;

use PHPUnit\Framework\TestCase;
use StepanDalecky\XmlElement\Element;
use StepanDalecky\XmlElement\Exceptions\UnexpectedXmlStructureException;

class ElementTest extends TestCase
{

	public function testGetChild()
	{
		$xml = '<test><child>value</child></test>';
		$element = Element::fromString($xml);

		self::assertSame('value', $element->getChild('child')->getValue());
	}

	public function testGetChildNotFound()
	{
		self::expectException(UnexpectedXmlStructureException::class);

		$xml = '<test><unknown>value</unknown></test>';
		$element = Element::fromString($xml);
		$element->getChild('child');
	}

	public function testGetChildMoreFound()
	{
		self::expectException(UnexpectedXmlStructureException::class);

		$xml = '<test><child>value</child><child></child></test>';
		$element = Element::fromString($xml);
		$element->getChild('child');
	}

	public function testGetChildren()
	{
		$xml = '<test><child>value</child><child></child></test>';
		$element = Element::fromString($xml);
		$children = $element->getChildren('child');

		$values = array_map(function (Element $element) {
			return $element->getValue();
		}, $children);

		self::assertSame(['value', ''], $values);
	}

	public function testGetChildrenNotFound()
	{
		self::expectException(UnexpectedXmlStructureException::class);

		$xml = '<test><unknown></unknown></test>';
		$element = Element::fromString($xml);
		$element->getChildren('child');
	}

	public function testGetChildrenNotFoundReturnEmpty()
	{
		$xml = '<test><unknown></unknown></test>';
		$element = Element::fromString($xml);

		self::assertSame($element->getChildren('child', false), []);
	}

	public function testGetValue()
	{
		$xml = '<test>value</test>';
		$element = Element::fromString($xml);

		self::assertSame('value', $element->getValue());
	}

	public function testGetAttribute()
	{
		$xml = '<test id="value" />';
		$element = Element::fromString($xml);

		self::assertSame('value', $element->getAttribute('id'));
	}

	public function testGetAttributeNotFound()
	{
		self::expectException(UnexpectedXmlStructureException::class);

		$xml = '<test unknown="value" />';
		$element = Element::fromString($xml);
		$element->getAttribute('id');
	}

	public function testGetAttributes()
	{
		$xml = '<test id="idValue" class="classValue" />';
		$element = Element::fromString($xml);

		self::assertSame([
			'id' => 'idValue',
			'class' => 'classValue',
		], $element->getAttributes());
	}

	public function testHasChildren()
	{
		$xml = '<test><child>value</child></test>';
		$element = Element::fromString($xml);
		self::assertSame($element->hasChildren(), true);

		$xml = '<test></test>';
		$element = Element::fromString($xml);
		self::assertSame($element->hasChildren(), false);

		$xml = '<test>value</test>';
		$element = Element::fromString($xml);
		self::assertSame($element->hasChildren(), false);
	}

	public function testHasAttributes()
	{
		$xml = '<test test-attribute="value">value</test>';
		$element = Element::fromString($xml);
		self::assertSame($element->hasAttributes(), true);

		$xml = '<test>value</test>';
		$element = Element::fromString($xml);
		self::assertSame($element->hasAttributes(), false);
	}
}
