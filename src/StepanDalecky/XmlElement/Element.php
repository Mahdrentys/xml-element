<?php
declare(strict_types = 1);

namespace StepanDalecky\XmlElement;

use StepanDalecky\XmlElement\Exceptions\UnexpectedXmlStructureException;

class Element
{

	/**
	 * @var \SimpleXMLElement
	 */
	private $xmlElement;

	public function __construct(\SimpleXMLElement $xmlElement)
	{
		$this->xmlElement = $xmlElement;
	}

	public static function fromString(string $xmlString): self
	{
		return new self(new \SimpleXMLElement($xmlString));
	}

	public function getChild(string $name): self
	{
		if (!isset($this->xmlElement->{$name})) {
			throw new UnexpectedXmlStructureException(sprintf(
				'There is no <%s> element nested in <%s> element.',
				$name,
				$this->getName()
			));
		}
		if ($this->xmlElement->{$name}->count() > 1) {
			throw new UnexpectedXmlStructureException(sprintf(
				'There are more <%s> elements nested in <%s>, only one was expected.',
				$name,
				$this->getName()
			));
		}

		/** @var \SimpleXMLElement $nestedXmlElement */
		$nestedXmlElement = $this->xmlElement->{$name};

		return new self($nestedXmlElement);
	}

	/**
	 * @param string $name
	 * @return self[]
	 */
	public function getChildren(string $name, bool $strictMode = true): array
	{
		if (!isset($this->xmlElement->{$name})) {
			if ($strictMode)
			{
				throw new UnexpectedXmlStructureException(sprintf(
					'There are no <%s> elements nested in <%s> element.',
					$name,
					$this->getName()
				));
			}
			else
			{
				return [];
			}
		}

		$elements = [];
		/** @var \SimpleXMLElement $xmlElement */
		foreach ($this->xmlElement->{$name} as $xmlElement) {
			$elements[] = new self($xmlElement);
		}

		return $elements;
	}

	public function getValue(): string
	{
		return (string) $this->xmlElement;
	}

	public function getAttribute(string $name): string
	{
		if (!isset($this->xmlElement[$name])) {
			throw new UnexpectedXmlStructureException(sprintf(
				'Attribute "%s" does not exists in <%s> element.',
				$name,
				$this->getName()
			));
		}

		return (string) $this->xmlElement[$name];
	}

	/**
	 * @return string[] [<attribute name> => <attribute value>, ...]
	 */
	public function getAttributes(): array
	{
		$attributes = [];
		/**
		 * @var string $name
		 * @var \SimpleXMLElement $xmlElement
		 */
		foreach ($this->xmlElement->attributes() as $name => $xmlElement) {
			$attributes[$name] = (string) $xmlElement;
		}

		return $attributes;
	}

	private function getName(): string
	{
		return $this->xmlElement->getName();
	}

	/**
	 * @return boolean
	 */
	public function hasChildren(): bool
	{
		foreach ($this->xmlElement as $key => $value)
		{
			if (!is_numeric($key))
			{
				// It is a child
				return true;
			}
		}

		return false;
	}

	/**
	 * @return boolean
	 */
	public function hasAttributes(): bool
	{
		return !empty($this->getAttributes());
	}
}
