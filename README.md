# Quickstart

Read XML in a more convenient way.

_xmlelement_ is:
* comfortable to use,
* easy to iterate on,
* keeping XML structure under control,
* predictable.

## Installation
Using [composer](https://getcomposer.org/):
```
composer require mahdrentys/xmlelement
```

## Usage

```xml
<grandpa name="Splinter" species="rat">
	<father name="Donatello">
		<sohn>me</sohn>
	</father>
	<uncle name="Michelangelo"></uncle>
	<uncle name="Leonardo"></uncle>
</grandpa>
```

```php
use StepanDalecky\XmlElement\Element;

$grandpa = Element::fromString($xmlString);

$grandpa->getAttributes(); // returns ['name' => 'Splinter', 'species' => 'rat']
$grandpa->getAttribute('name'); // returns 'Splinter'

$grandpa->getChild('father')
	->getChild('sohn')
	->getValue(); // Me
$grandpa->getChild('mother'); // throws an exception - not found
$grandpa->getChild('uncle'); // throws an exception - more children found

$grandpa->getChildren('uncle'); // returns an array consisting of two elements
$grandpa->getChildren('aunt'); // throw an exception because there is no aunt children
$grandpa->getChildren('aunt', false); // return an empty array because $strictMode is false

$grandpa->hasChildren(); // returns true
$grandpa->getChildren('uncle')[0]->hasChildren(); // returns false

$grandpa->hasAttributes(); // returns true
$grandpa->getChild('father')->getChild('sohn')->hasAttributes(); // returns false
```